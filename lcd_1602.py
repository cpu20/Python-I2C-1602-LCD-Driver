"""
    Python-I2C-1602-LCD-Driver
    Copyright (C) 2018  Tijl Schepens

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""@package docstring
This module can be used to 1602 LCD.

Upon initialization, an interface object must be given to drive the LCD.
The interface object determines how the LCD is driven. Over SPI, I2C,
parallel interface,...
"""

import time
import smbus2
import threading

class Lcd1602:
    """ Class used to drive a 1602 LCD using a PCF8574 IC
    """
    def __init__(self, interface):
        """ The constructor initializes private variables and opens the
        serial bus using the smbus module.

        @param interface  The interface object implementing the writeLCD
                          method and controls the backlight
        """
        ## Buffer holding the strings currently displayed on the LCD
        self.text_buffer = [[' '] * 16] + [[' '] * 16]
        ## Keep track of the cursor line position
        self.cursor_line = 0  
        ## Keep track of the cursor row position
        self.cursor_row = 0
        ## Interface object
        self.intf = interface
        # Set all options to sensible values
        ## 0 = one line display mode, 1 = two line display mode.
        self.lines = 1
        ## Move the cursor to the right when set to 1 and to the left when set to 0.
        self.curs_lr = 1
        ## If this is set to 1 the entire display content is shifted left or right depending on CursLR.
        self.shfit_disp = 0
        ## Turn the entire display on or off (1 = ON, 0 = OFF). Displayed data is saved in DDRAM when turned off.
        self.on_off = 1
        ## When set to 1 the cursor is turned on.
        self.curs_on_off = 0
        ## The cursor will blink if set to 1 and CursOnOff is set to 1 (cursor is visible).
        self.blink_on_off = 0
        ## Is the backlight enabled or disabled? Enabled by default.
        self.backlight = 1
        ## Lock used for the shifting threads
        self.lock = threading.Lock()
        ## Two threads used for shifting the text
        self.shifter_threads = [None, None] # Initialized with none to indicate that no thread is assigned

    def initLCD(self, curs_lr=1, shift_disp=0, on_off=1,
                curs_on_off=0, blink_on_off=0):
        """ Initialize the 1602 LCD with the given parameters.

        @param curs_lr      Move the cursor to the right when set to 1 and to the left when
                            set to 0.
        @param shift_disp   If this is set to 1 the entire display content is shifted left or
                            right depending on CursLR.
        @param on_off       Turn the entire display on or off (1 = ON, 0 = OFF). Displayed data
                            is saved in DDRAM when turned off.
        @param curs_on_off  When set to 1 the cursor is turned on.
        @param blink_on_off The cursor will blink if set to 1 and CursOnOff is set to 1 (cursor
                            is visible).
        """
        # Save variables
        self.lines = 2
        self.curs_lr = curs_lr
        self.shfit_disp = shift_disp
        self.on_off = on_off
        self.curs_on_off = curs_on_off
        self.blink_on_off = blink_on_off
        data = [0x08, 0x01, ((curs_lr<<1) + shift_disp + 0x4), 
                ((on_off<<2) + (curs_on_off<<1) + blink_on_off + 0x8)]

        for i in data:
            self.intf.writeLCD(i)
            time.sleep(0.005)

    def enableBacklight(self):
        self.intf.backlight = True

    def disableBacklight(self):
        self.intf.backlight = False

    def enableLCD(self):
        """ Enable the display. """
        self.intf.writeLCD((self.curs_on_off<<1) + 0x4)
        self.on_off = 1

    def disableLCD(self):
        """ Disable the display. """
        self.intf.writeLCD((self.curs_on_off<<1))
        self.on_off = 0

    def isLCDEnabled(self):
        """ Check if the display is enabled or disabled.

        @retval   1 = Display is enabled, 0 = Display is disabled
        """
        return self.on_off

    def setCursor(self, line, row):
        """ Set the cursor to a specific location.

        @param line   Line 0 or 1. (Will automatically be 0 in one line mode)
        @param row    Row 0-15.
        """
        if not self.lines:
            self.intf.writeLCD(0x80 + row)
            self.cursor_line = 0
        else:
            self.intf.writeLCD(0x80 + (line << 6) + row)
            self.cursor_line = line
        # Update the cursor position
        self.cursor_row = row

    def printToLCD(self, char):
        """ Print a character to the LCD at the current cursor position.

        @param char   The character to print.
        """
        self.intf.writeLCD(ord(char), rs=1)

        self.updateStrings(char)
        self.updateCursorPos(0, 1)

    def printStringToLCD(self, text):
        """ Print a string to the LCD at the current cursor position. If
            the string is longer than the line width of the display (16
            characters) the function will continue printing on the other
            line.

        @param text   The string to print.
        """
        for char in text:
            self.printToLCD(char)

    def printStringOnOneLine(self, line, text, initial_shift=0):
        """ Print a string on one line of the display. If the string is
            longer than the display width, this function will stop
            printing at the end of the line. The string is fully
            buffered and can be shifted x characters at a time using
            shiftString().

        @param line           Line to print the string on.
        @param text           The string to be printed and buffered for shifting.
        @param initial_shift  Shift the string before printing it.
        """
        if not self.lines:
            line = 0
        self.text_buffer[line] = [text[0]]
        # The whole string is buffered so that the string can be shifted.
        i = 0
        for char in text:
            if i is not 0:
                self.text_buffer[line] += [char]
            i += 1
        # Give the string it's initial shift and print it.
        self.shiftString(line, initial_shift)

    def printStringAndRotate(self, line, text, shift_interval=0.65, shift_positions=-1):
        """ Print a string on a line of the display. If the given string
            is longer than 16 characters the string will automatically
            be rotated around.

        @param line             Line to print the stirng on.
        @param text             The string to be printed.
        @param shift_interval   Interval between two shift operations.
        @param shift_positions  The amount of positions the string is shifted over.
        """
        if not self.lines:
            line = 0
        # If the current string is being shifted, stop the operation.
        self.stopShifting(line)

        if len(text) > 16:
            self.printStringOnOneLine(line, text)
            self.startShifting(line, shift_interval, shift_positions)
        else:
            self.refreshLine(line, text)

    def shiftString(self, line, pos):
        """ Shift the string on one of the lines over a given number
            of characters.

        @param line         Which line is the string on
        @param characters   The amount of characters to shift over
        """
        if not self.lines:
            line = 0

        pos = -pos % len(self.text_buffer[line])
        self.text_buffer[line] = self.text_buffer[line][pos:] + self.text_buffer[line][:pos]
        self.setCursor(line, 0)
        self.printStringToLCD(self.text_buffer[line][:16])

    def startShifting(self, line, shift_interval, shift_positions):
        """ Start shifting the string that has been printed with
            printStringOnOneLine().

        @param line             Line to start the string shifting on.
        @param shift_interval   Interval between two shift operations.
        @param shift_positions  The amount of positions the string is shifted over.
        """
        if not self.lines:
            line = 0
        # Check if no thread is already active. Active threads should be stopped before spawning a new one.
        if self.shifter_threads[line] is None:
            # Spawn a thread that rotates the text
            self.shifter_threads[line] = self.ShifterThread(self, line, shift_interval, shift_positions)
            self.shifter_threads[line].start()

    def stopShifting(self, line):
        """ Stop shifting the string that has been printed with
            printStringOneOneLine().

        @param line             Line to stop the string shifting.
        """
        if not self.lines:
            line = 0
        # Check if a thread is active before trying to stop it.
        if self.shifter_threads[line] is not None:
            # Stop the running thread.
            if not self.shifter_threads[line].stopped():
                self.shifter_threads[line].stop()
                self.shifter_threads[line].join()
                self.shifter_threads[line] = None # Set to None to indicate that no thread is active.

    def updateStrings(self, char):
        """ Update the string buffers when a character is printed to
            the display. WARNING: Call this function before updating the
            cursor position!!!

        @param char   The character that has been printed to the display.
        """
        self.text_buffer[self.cursor_line][self.cursor_row] = char

    def emptyTextBuffer(self, line):
        """ Empty one of the text buffer lines and replace the contents
            with spaces.

        @param line   The line to be emptied.
        """
        if not self.lines:
            line = 0

        self.text_buffer[line] = [' '] * 16

    def updateCursorPos(self, lines, rows):
        """ Update the cursor position internally after a certain operation.

        @param lines    The amount of lines the cursor advanced.
        @param rows     How many rows did the cursor advance?
        """
        self.cursor_row += rows
        self.cursor_line += lines
        if self.cursor_row >= 16:
            self.cursor_line += int(self.cursor_row / 16)
            self.cursor_row -= 16 * int(self.cursor_row / 16)
        if self.cursor_line >= 2:
            if self.cursor_line % 2:
                self.cursor_line = 0
            else:
                self.cursor_line = 1

    def clearLCD(self):
        """ Clear the LCD completely. Line buffers are emptied and cursor
            position is reset to 0,0. """
        # If strings are being shifted stop the shifting
        self.stopShifting(0)
        self.stopShifting(1)
        # Clear the LCD
        self.intf.writeLCD(0x1)
        time.sleep(0.15) # The clear operation takes 150ms

        self.text_buffer = [[' '] * 16] + [[' '] * 16]
        self.cursor_line = 0
        self.cursor_row	= 0

    def refreshLine(self, line, text):
        """ Refresh a given line with a new string. The string is printed
            using printStringToLCD() and thus the string is not truncated
            at 16 characters.

        @param line   The line to be replaced.
        @param text   The string to be printed.
        """
        if not self.lines:
            line = 0
        self.setCursor(line, 0)
        self.emptyTextBuffer(line)
        # Clear the line by printing spaces.
        self.printStringToLCD("                ")
        self.setCursor(line, 0)
        # Print the new string.
        self.printStringToLCD(text)

    def getCursorPos(self):
        """ Get the current cursor position.

        @retval cursor_line   The line where the cursor is currently located.
        @retval cursor_row    Current row where the cursor is located.
        """
        return self.cursor_line, self.cursor_row

    def getDisplayedText(self):
        """ Get the currently displayed strings.

        @retval text_buffer[0]   Text displayed on the first line of the display.
        @retval text_buffer[1]   String displayed on the second line of the display.
        """
        return self.text_buffer[0], self.text_buffer[1]

    class ShifterThread(threading.Thread):
        """ Threading class used to automatically shift the text on a
            certain line of the display. """
        def __init__(self, lcd, line, shift_interval, shift_positions):
            """ Default constructor.

            @param lcd              The object used to manipulate the LCD from the class I2cLcd.
            @param line             The line to shift the text.
            @param shift_interval   Interval between text shifts.
            @param shift_positions  Number of positions to shift the text over. 
            """
            super(Lcd1602.ShifterThread, self).__init__()
            self.thread_stopped = threading.Event()
            self.thread_stopped.set()
            self.lcd = lcd
            self.line = line
            self.shift_interval = shift_interval
            self.shift_positions = shift_positions

        def run(self):
            """ The thread shifting the text until it is explicitly stopped. """
            self.thread_stopped.clear()
            while not self.stopped():
                if self.stopped():
                    return
                time.sleep(self.shift_interval)
                self.lcd.lock.acquire()
                self.lcd.shiftString(self.line, self.shift_positions)
                self.lcd.lock.release()
            
        def stop(self):
            """ Stop the shifting thread. """
            self.thread_stopped.set()

        def stopped(self):
            """ Used to check if the shifting thread is stopped.

            @retval True:   The thread is stopped.
            @retval False:  The thread is running. 
            """
            return self.thread_stopped.is_set()

        def set_interval(self, shift_interval):
            """ Change the shifting interval.

            @param shift_interval   The interval between two shift operations.
            """
            self.shift_interval = shift_interval

        def set_positions(self, shift_positions):
            """ Change the amount of positions over which the text is shifted.

            @param shift_positions  Positions the text is shifted over every interval.
            """
            self.shift_positions = shift_positions

        def set_line(self, line):
            """ Change the line on which the text is shifted.

            @param line   The line on which the text is shifted.
            """
            self.line = line
