"""
    Python-I2C-1602-LCD-Driver
    Copyright (C) 2020  Tijl Schepens

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""@package docstring
This module can be used to drive 1602 LCDs over SPI.
"""

import time
import gpiozero
import threading

class SpiLcdIntf:
    """ Class used to drive a 1602 LCD over SPI.
    """
    def __init__(self, csb_pin, sdi_pin, sdo_pin, scl_pin, lines, font_type):
        """ The constructor opens the serial bus.
        """
        ## Lock used for the communication
        self.lock = threading.Lock()
        ## Used to drive csb
        self.csb = gpiozero.OutputDevice(csb_pin, initial_value=True)
        ## Used to drive sdi
        self.sdi = gpiozero.OutputDevice(sdi_pin)
        ## Used to drive sdo
        self.sdo = gpiozero.OutputDevice(sdo_pin)
        ## Used to drive scl
        self.scl = gpiozero.OutputDevice(scl_pin, initial_value=True)

        # Initialize the display in serial
        data = 0x30 + ((lines<<3) + (font_type<<2))

        self.writeLCD(data, 0x0)
        time.sleep(0.005)

    def writeLCD(self, data, rs=0, rw=0):
        """ Send data and configuration bits to the LCD.
            Pinout: DB7 - DB4 BL EN RW RS

        @param data   The data to be sent to the LCD (DB7 - DB4)
        @param config The configuation bits to send to the LCD (RS/RW)
        """
        self.lock.acquire()
        self.csb.off()

        # Data is clocked in on the rising edge
        self.scl.off()
        if rs:
            self.sdo.on()
        else:
            self.sdo.off()
        self.scl.on()

        self.scl.off()
        if rw:
            self.sdo.on()
        else:
            self.sdo.off()
        self.scl.on()

        for i in range(7, -1, -1):
            self.scl.off()
            if (data >> i) & 0x1:
                self.sdo.on()
            else:
                self.sdo.off()
            self.scl.on()

        self.csb.on()
        self.lock.release()

    @property
    def backlight(self):
      return self._backlight

    @backlight.setter
    def backlight(self, backlight_on):
        self._backlight = int(backlight_on)
