"""
    Python-I2C-1602-LCD-Driver
    Copyright (C) 2018  Tijl Schepens

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""@package docstring
This program can be used to test the I2cLcd class
and see if it works with your hardware.
"""

import sys
import time
sys.path.append('../')
import lcd_1602
import i2c_intf

# Create an object. Port is standard 1.
i2c = i2c_intf.I2cLcdIntf(0x38, lines=2, font_type=1)
lcd = lcd_1602.Lcd1602(i2c)
# Initialize the display using the standard configuration values.
lcd.initLCD()
# Print a simple "Hello world!" string on the first line of the display.
lcd.printStringToLCD("Hello world!")
time.sleep(2)
lcd.disableBacklight()
time.sleep(2)
lcd.enableBacklight()
