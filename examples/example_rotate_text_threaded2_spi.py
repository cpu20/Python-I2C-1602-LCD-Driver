"""
    Python-I2C-1602-LCD-Driver
    Copyright (C) 2018  Tijl Schepens

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""@package docstring
This program can be used to test the I2cLcd class
and see if it works with your hardware.
"""

import sys
import time
import threading
sys.path.append('../')
import lcd_1602
import spi_intf

rotate = True

text_line0 = "Rotation example text!!!"
text_line1 = "!!!txet elpmaxe noitatoR"

spi = spi_intf.SpiLcdIntf(17, 9, 10, 11, lines=1, font_type=0)
lcd = lcd_1602.Lcd1602(spi)
# Initialize the display using the standard configuration values.
lcd.initLCD()
# Print a string that is longer than 16 characters.
lcd.printStringOnOneLine(0, text_line0)
lcd.printStringOnOneLine(1, text_line1)
# Rotate the text using the internal threading mechanism
lcd.startShifting(0, 0.2, 1)
lcd.startShifting(1, 0.7, -2)

time.sleep(20)

lcd.stopShifting(0)
lcd.stopShifting(1)
