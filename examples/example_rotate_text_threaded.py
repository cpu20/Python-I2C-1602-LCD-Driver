"""
    Python-I2C-1602-LCD-Driver
    Copyright (C) 2018  Tijl Schepens

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""@package docstring
This program can be used to test the I2cLcd class
and see if it works with your hardware.
"""

import sys
import time
import threading
sys.path.append('../')
import lcd_1602
import i2c_intf

rotate = True

def textRotator():
    # Rotate the text indefinitely
    shift = 0
    direction = 1
    while(rotate == True):
        if shift == len(text_line0):
            direction = -direction
            shift = 0

        time.sleep(0.7)
        lcd.shiftString(0, direction)
        lcd.shiftString(1, -direction)

        shift += 1
    return

text_line0 = "Rotation example text!!!"
text_line1 = "!!!txet elpmaxe noitatoR"
threads = []

i2c = i2c_intf.I2cLcdIntf(0x38, lines=2, font_type=1)
lcd = lcd_1602.Lcd1602(i2c)
# Initialize the display using the standard configuration values.
lcd.initLCD()
# Print a string that is longer than 16 characters.
lcd.printStringOnOneLine(0, text_line0)
lcd.printStringOnOneLine(1, text_line1)
# Start the rotation thread
t = threading.Thread(target=textRotator)
threads.append(t)
t.start()
# End the thread after 60 seconds
time.sleep(60)
rotate = False
