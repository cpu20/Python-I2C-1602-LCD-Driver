"""
    Python-I2C-1602-LCD-Driver
    Copyright (C) 2020  Tijl Schepens

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""@package docstring
This module can be used to drive 1602 LCDs over I2C.

The I2C module is based on a PCF8574 which can be bought in China very cheaply.
"""

import time
import smbus
import threading

class I2cLcdIntf:
    """ Class used to drive a 1602 LCD using a PCF8574 IC
    """
    def __init__(self, address, lines, font_type, port=1):
        """ The constructor opens the serial bus using the smbus module.

        @param address  I2C address of the PCF8574
        @param port     If there are multiple I2C lines, port selects the apprioriate ones
        @param lines        0 = one line display mode, 1 = two line display mode.
        @param font_type    1 = 5x11 dots format display mode, 0 = 5x8 dots format display mode
        """
        ## The I2C address of the PCF8574
        self.addr = address
        ## SMBUS object to communicate over I2C
        self.bus = smbus.SMBus(port)
        # Is the backlight enabled or not?
        # This is also controlled through the I2C interface.
        self._backlight = False
        ## Lock used for the communication
        self.lock = threading.Lock()

        # Place the pins in a known state
        self.intf.writeLCD(0x0, 0x0)

        # Initialize the display in 4-bit mode
        data = [0x33, 0x32, (0x2 << 4) + ((lines<<3) + (font_type<<2))]

        for i in data:
            self.writeLCD(i, 0x0)
            time.sleep(0.005)

    def writeLCD(self, data, rs=0, rw=0):
        """ Send data and configuration bits to the LCD.
            Pinout: DB7 - DB4 BL EN RW RS

        @param data   The data to be send to the LCD (DB7 - DB4)
        @param config The configuation bits to send to the LCD (RS/RW)
        """
        backlight = int(self.backlight) << 3
        byte = (data & 0xF0) + (rw << 1) + rs + backlight

        # Add 0x4 to put the enable pin high
        self.lock.acquire()
        self.bus.write_byte(self.addr, byte + 0x4)
        time.sleep(0.0001) # Give the display time to read the data
        self.bus.write_byte(self.addr, byte)
        self.lock.release()

        byte = ((data & 0x0F) << 4) + config + backlight
        # Add 0x4 to put the enable pin high
        self.lock.acquire()
        self.bus.write_byte(self.addr, byte + 0x4)
        time.sleep(0.0001) # Give the display time to read the data
        self.bus.write_byte(self.addr, byte)
        self.lock.release()

    def readLCD(self):
        """ Read data back from the bus. """
        self.lock.acquire()
        data = self.bus.read_byte(self.addr)
        self.lock.release()

        return data

    @property
    def backlight(self):
      return self._backlight

    @backlight.setter
    def backlight(self, backlight_on):
        if backlight_on:
            """ Enable the display backlight. """
            self._backlight = 1
            self.intf.writeLCD(self.intf.readLCD()>>4, 0x0)
        else:
            """ Disable the display backlight. """
            self._backlight = 0
            self.intf.writeLCD(self.intf.readLCD()>>4, 0x0)
