# Python 1602 LCD driver
This Python module provides a driver to control an 1602 display. 
An I2C module can be used to control the display. The module is based around the PCF8574 and can be easily found on EBay/Aliexpress.
It is also possible to control the display directly through a serial interface.

## Structure
* Examples    Some examples to show how to use the driver.
* html        HTML documentation generated by Doxygen.
* latex       Latex documentation generated by Doxygen.
* lcd_1602.py The real deal.
* i2c_intf.py Code to drive the LCD using an I2C PCF8574.
* spi_intf.py Code to drive the LCD directly using a serial interface.

## Usage
* Import the module `import lcd_1602`
* Instantiate your desired communication interface. `i2c = i2c_intf.I2cLcdIntf()` or `spi = spi_intf.SpiLcdInf()`
* Create an object for the lcd class `myLCDObject = lcd_1602.Lcd1602()`
* Initialize the LCD `myLCDObject.initLCD()`
* See the examples and the documentation to see how to use the provided methods.

## In action
Here are some pictures of the 1602 display with the rotate text example in action:

![Rotate1](https://i.imgur.com/5Cvsl7L.jpg?1)
![Rotate2](https://i.imgur.com/XUB2SFK.jpg?1)

## Contributing
If you want to contribute to this driver just drop me a message or submit a pull request.
## Bugs/Problems
If you discover any bugs or have any problems don’t hesitate to contact me. Or add a pull request.
